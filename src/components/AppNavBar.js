import {Nav, Navbar, NavDropdown, Container, Form, Button} from 'react-bootstrap';
import React from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { IoCartOutline } from "react-icons/io5";
import { AiOutlineUser } from "react-icons/ai";



const AppNavBar = () => {
    let user = JSON.parse(localStorage.getItem('user-info'));
    const history = useHistory();
    function logOut() 
    {
        localStorage.clear()
        history.push('/login')
    }
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <Navbar.Brand as={NavLink} to="/">
                <img src="/pics/denwa2.png" width="70" height="70" className="d-inline-block align-top justify-content-start" alt="not found"/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-center">
                        <Nav className="me-auto" id="navOne">
                            {/* <Nav.Link id="featureAnchor"><a href="#">Features</a></Nav.Link> */}
                            <Nav.Link id="featureAnchor" as={NavLink} to="/home">Features</Nav.Link>
                            <Nav.Link id="productAnchor"><a href="#">Products</a></Nav.Link>
                            <NavDropdown title="Categories" id="collasible-nav-dropdown" menuVariant="dark">
                                <NavDropdown.Item as={NavLink} to="/products">Mobile Phones</NavDropdown.Item>
                            </NavDropdown>
                            <Nav>
                                <Nav.Link as={NavLink} to="/contacts">Contacts</Nav.Link>
                            </Nav>
                        </Nav>
                    </Navbar.Collapse>
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                        <Nav className="me-auto">
                            <NavDropdown title={<AiOutlineUser size="1.3em"/>} id="collasible-nav-dropdown"> 
                                <NavDropdown.Item as={NavLink} to="/login">Login</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} to="/register">Create an account</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item onClick={logOut}>Logout</NavDropdown.Item>
                            </NavDropdown>
                        <Nav.Link id="cartIcon"> 
                            <IoCartOutline size="1.3em" /> 
                        </Nav.Link>       
                        </Nav>
                        <Form className="d-flex">
                            <Form.Control type="search" placeholder="Search" className="me-2 ml-3" aria-label="Search" />
                            <Button variant="outline-light">Search</Button>
                        </Form>
                    </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default AppNavBar;