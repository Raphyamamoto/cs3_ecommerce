import Card from 'react-bootstrap/Card';
import React from 'react';
import { Link } from 'react-router-dom';

const SingleProduct = ({productsInfo}) => {
  const {_id, productName, link, categories, description, price} = productsInfo;
  return(
      <Card>
          <Card.Body>
            <div id="imageCustom">
              <img src={link} alt="not found" />
            </div>
              <Card.Title> {productName}</Card.Title>
              <Card.Subtitle> Categories: </Card.Subtitle>
              <Card.Text>
                  {categories}
              </Card.Text>
              <Card.Subtitle> Product Description: </Card.Subtitle>
              <Card.Text>
                  {description}
              </Card.Text>
              <Card.Subtitle> Product Price: </Card.Subtitle>
              <Card.Text> PHP {price} </Card.Text>
              <Link to={`/item/${_id}`}>
              <button type="button"> Buy </button>
            </Link>
          </Card.Body>
      </Card>
  )
}

export default SingleProduct;