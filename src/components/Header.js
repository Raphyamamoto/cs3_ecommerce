// import React, { useEffect, useState, Fragment } from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import { AppBar, IconButton, Toolbar, Collapse } from '@material-ui/core';
// import HomeIcon from "@material-ui/icons/Home";
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import { Link as Scroll } from 'react-scroll';
// import { Navlink, Route, Switch, BrowserRouter as Router, Link } from 'react-router-dom';
// import Home from '../pages/Home';



// const useStyles = makeStyles((theme) => ({
//   root: {
//     display: 'flex',
//     justifyContent: 'center',
//     alignItems: 'center',
//     height: '100vh',
//     fontFamily: 'Poppins',
//   },
//   appbar: {
//     background: 'none',
//   },
//   appbarWrapper: {
//     width: '80%',
//     margin: '0 auto',
//   },
//   appbarTitle: {
//     flexGrow: '1',
//   },
//   icon: {
//     color: '#fff',
//     fontSize: '2rem',
//   },
//   colorText: {
//     color: '#e6e4e1',
//   },
//   container: {
//     textAlign: 'center',
//   },
//   title: {
//     color: '#fff',
//     fontSize: '4.5rem',
//   },
//   goDown: {
//     color: '#e6e4e1',
//     fontSize: '4rem',
//   },
// }));
// export default function Header() {
//   const classes = useStyles();
//   const [checked, setChecked] = useState(false);
//   useEffect(() => {
//     setChecked(true);
//   }, []);
//   return (
//     <Router>
//         <div className={classes.root} id="header">
//         <AppBar className={classes.appbar} elevation={0}>
//             <Toolbar className={classes.appbarWrapper}>
//             <div className={classes.appbarTitle}>
//                 <img src="/pics/crest1.png" width="70" height="70" className="d-inline-block align-top" alt="not found"/>
//             </div>
//             <Link to="/home" className={classes.link}>
//                 <IconButton>
//                     <HomeIcon className={classes.icon} />
//                 </IconButton>
//             </Link>  
//             <Switch>
//                 <Route exact path="/home" component={Home} />
//             </Switch>    
//             </Toolbar>
//         </AppBar>

//         <Collapse
//             in={checked}
//             {...(checked ? { timeout: 1000 } : {})}
//             collapsedHeight={50}
//         >
//             <div className={classes.container}>
//             <h1 className={classes.title}>
//                 Welcome <span className={classes.colorText}>to </span><br />
//             </h1>
//             <div id="logo" className="mt-2">
//                 <img src="/pics/denwa2.png" height="300" width="300" />
//             </div>
//             <Scroll to="items-to-choose" smooth={true}>
//                 <IconButton>
//                 <ExpandMoreIcon className={classes.goDown} />
//                 </IconButton>
//             </Scroll>
//             </div>
//         </Collapse>
//         </div>
//     </Router>
//     );
// }
