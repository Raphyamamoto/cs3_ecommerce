const items = [
    {
      title: 'Apple',
      description:
        "Think Different --- But Not To Different",
      imageUrl: '/pics/apple.jpg',
      time: 1500,
    },
    {
      title: 'Samsung',
      description:
        'Inspire the World, Create the future',
      imageUrl: '/pics/samsung1.png',
      time: 1500,
    },
    {
        title: 'Huawei',
        description:
            'Make It Possible',
        imageUrl: '/pics/huawei.jpg',
        time: 1500,
    },
    {
        title: 'Oppo',
        description:
            'Designed for Life',
        imageUrl: '/pics/oppo1.png',
        time: 1500,
    },
    {
        title: 'Vivo',
        description:
            'Perfect Selfie',
        imageUrl: '/pics/vivo1.png',
        time: 1500,
    },
  ];
  
  export default items;
  