import { Row, Col, Button } from 'react-bootstrap';
import React from 'react';
import { NavLink } from 'react-router-dom';

export default function LandingError() {
    return (
        <Row className="text-center">
            <Col>
            <div id="logoError">
                <img src="/pics/404.png" height="500" width="500" alt="404 Not Found" />
            </div>
            <div>
                <h1>OOOOOOOOPSSSSSS!</h1>
                <h4>The Page you are looking for can't be found</h4>
            </div>
            <div className="mt-5">
                <Button as={NavLink} to="/home" variant="outline-dark"> Go Back
                </Button>{' '}
            </div>
            </Col>
        </Row>
    )
}