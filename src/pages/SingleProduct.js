import { useState, useEffect } from 'react';
import React, {Fragment} from 'react';
// import SingleProduct from '../components/SingleProductComponent';
import { useParams } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';

export default function GetSingleProduct() {
    //STATE HOOK
    const params = useParams();
    console.log(params.id)

    const [singleProduct, setSingleProduct] = useState([]);
    

    useEffect(() => {
       
        fetch(`https://evening-earth-18907.herokuapp.com/product/selected/${params.id}`).then(outcomeNgFetch => outcomeNgFetch.json()).then(convertedData => {
         // console.log(convertedData)
             //the map method will loop through each individual course object inside our array and tweak then 1 by 1
             setSingleProduct(convertedData)
             console.log(convertedData)
        }
 
   )},[params])
    return(
        <Fragment>
            <Row id="singleProductRow">
                <Col id="singleProductCol1">
                    <img id="imageSingleProduct" src={singleProduct.link} alt="Not Found" />
                </Col>
                <Col id="singleProductCol2">
                    <div id="singleProductName">
                        {singleProduct.productName}
                    </div>
                    <div id="singleProductDescription">
                        {singleProduct.description}
                    </div>
                    <div id="singleProductCategories">
                        "{singleProduct.categories}"
                    </div>
                    <div id="singleProductPrice">
                        Php {singleProduct.price}   
                    </div>
                    <div>
                        <button className="mt-3 mb-2" id="buttonP" type="button"> Add to Cart </button>
                    </div>
                    <div>
                        <button className="mt-2" id="buttonP" type="button"> Checkout </button>
                    </div>
                </Col>
            </Row>
        </Fragment>
    )
};


