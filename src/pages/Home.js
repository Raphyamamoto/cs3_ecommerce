import Banner from '../components/Banner';
import { Button } from 'react-bootstrap';
import React from 'react';
import { NavLink } from 'react-router-dom';
import '../App.css';

let item = {
    title: "THE BEST DEALS THAT YOU WILL EVER FIND!",
    paragraph: "'We offer the cheapest but quality products and services that you could ever find on the net.'"
}
export default function Bahay() {
    return (
        <div id="mainDiv">
            {/* <div id="logo">
                <img src="/pics/denwa.png" height="500" width="500" />
            </div> */}
            <div>
                <div id="homeBanner">
                    <Banner props={item} />
                </div>
                <div id="mainHome">
                    <Button className="mb-5 mt-4" as={NavLink} to="/products" variant="outline-light"> Shop Now
                    </Button>{' '}
                </div>
            </div>
            <div>

            </div>
        </div>
    )
}