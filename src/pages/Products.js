import { useState, useEffect } from 'react';
import React, {Fragment} from 'react';
import Hero from '../components/Banner';
import ImageCard from '../components/ImageCard';



let infoProducts = {
    title: "Welcome to the Products Page!",
    paragraph: "Shop Now!"
};
export default function GetProducts() {
    //STATE HOOK
    const [products, setProducts] = useState([]); 

    useEffect(() => {
         fetch('https://evening-earth-18907.herokuapp.com/product/all').then(responseFetch => responseFetch.json()).then(convertedData => {
            // console.log(convertedData); 

            setProducts(convertedData.map(product => {
                return(
                    //the map method will store/contain each subject in their own individual component
                    <ImageCard key={product._id} productsInfo={product}/>
                )
            }))
        })
    });
    console.log(products)
    return(
        <Fragment>
            <Hero props={infoProducts} />
            {products}
        </Fragment>
    )
};


