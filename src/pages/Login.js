import React from 'react';
import { Form, Button, Container } from 'react-bootstrap';
// import { Navlink } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';
import { useHistory} from 'react-router-dom';

export default function Signin() {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const authenticate = (e) => {
        e.preventDefault()//refreshing page
        
        fetch(`https://evening-earth-18907.herokuapp.com/user/login/`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data === false) {
                Swal.fire({
                    title: `Invalid Login!`,
                    icon: 'error',
                })
            } else {
                localStorage.setItem('access', data.accessToken)
                Swal.fire({
                    title: `You are now logged in!`,
                    icon: 'success',
                    text: 'Enjoy Shopping!'
                })
                history.push('/home');  
            }
        })
    };

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password]);


    return(
        <Container id="signInContainer">
            <div className="text-center">
                <h3> Sign In </h3>

                                        {/* Form */}
                    <div id="formId">
                                        {/* Email */}
                    <Form id="signInForm" onSubmit={e => authenticate(e)}>
                        <Form.Group controlId="email" md="3">
                            <Form.Label id="emailAd">Email Address</Form.Label>
                            <em id="asterisk1">*</em>
                            <Form.Control type="email" placeholder="Email" value={email} onChange={event => setEmail(event.target.value)} required />
                        </Form.Group>
                                        {/* Password */}
                        <Form.Group controlId="password" md="3">
                            <Form.Label id="passwordEmail">Password</Form.Label>
                            <em id="asterisk2">*</em>
                            <Form.Control type="password" placeholder="password" onChange={event => setPassword(event.target.value)} required />
                        </Form.Group>

                        <p id="required"> Required fields</p>
                        <em id="asterisk3">*</em>

                                        {/* Buttons */}
                        <div>
                        { isActive ? 
                        <Button variant="outline-dark" type="submit" id="signInButton" className='btn btn-block mt-2' value="Sign In">
                            SIGN IN
                        </Button>
                        :
                        <Button variant="outline-dark" type="submit" id="signInButton" className='btn btn-block mt-2' value="Sign In" disabled>
                            SIGN IN
                        </Button>
                        }
                        </div>
                        <div>
                            <p className="mb-3"> 
                            <span> ------ OR ------ </span>
                            </p>
                        </div>
                        <div>
                        <a href="./Register">
                        <Button variant="outline-dark" id="createAcctButton" className='btn btn-block'>
                            CREATE AN ACCOUNT
                        </Button>
                        </a>
                        </div>
                    </Form>
                    </div>
             </div>
        </Container>
    )

}