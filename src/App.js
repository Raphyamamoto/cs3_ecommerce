import './App.css';
import React from 'react';
import Navbar from './components/AppNavBar';
import Footer from './components/AppFooter';
// import Landing from './pages/Landing';
import Bahay from './pages/Home';
import GetProducts from './pages/Products';
import SingleProduct from './pages/SingleProduct';
import Login from './pages/Login';
// import Logout from './pages/Logout';
import Contact from './pages/Contact';
import Register from './pages/Register';
import Error from './pages/Error';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';

const App = () => {
  
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path="/features" component={Bahay} />
        <Route exact path="/home" component={Bahay} />
        {/* <Route exact path="/" component={Landing} /> */}
        <Route exact path="/products" component={GetProducts} />
        <Route exact path="/item/:id" component={SingleProduct} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        {/* <Route exact path="/logout" component={Logout} /> */}
        <Route exact path="/contacts" component={Contact} />
        <Route component={Error} />
      </Switch>
    <Footer />
    </Router>
  )
}

export default App;

// import React from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import { CssBaseline } from '@material-ui/core';
// import Header from './components/Header';
// import Brands from './components/Brands';
// import ImageLanding from '../public/pics/landing3.jpg';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     minHeight: '100vh',
//     backgroundImage: `url(${ImageLanding})`,
//     backgroundRepeat: 'no-repeat',
//     backgroundSize: 'cover',
//   },
// }));
// export default function App() {
//   const classes = useStyles();
//   return (
//     <div className={classes.root}>
//       <CssBaseline />
//       <Header />
//       <Brands />
//     </div>
//   )
  
// }

